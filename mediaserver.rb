this_dir = File.expand_path(File.dirname(__FILE__))
lib_dir = File.join(this_dir, 'lib')
$LOAD_PATH.unshift(lib_dir) unless $LOAD_PATH.include?(lib_dir)

require 'grpc'
require 'media_server_services_pb'
require 'fileutils'

class MediaServiceServer < MediaService::Service
    
    def subir_imagen(chunk)
        bytesfile = []
        dir = ""
        puts "Recibiendo archivo"

        chunk.each_remote_read do |c|

            if c.id_escucha.nil? || c.id_artista.nil? || c.id_album.nil? || c.id_cancion.nil?
                raise GRPC::InvalidArgument.new
            end
            
            dir = "/home/vagrant/bin/res/#{c.id_escucha}/#{c.id_artista}/#{c.id_album}/#{c.id_cancion}"
            FileUtils.mkdir_p(dir) unless Dir.exists?(dir)
            
            bytesfile << c.content       
        end

        file = open(File.join(dir,"cover.jpg"), 'wb')
        
        bytesfile.each do |bytes| 
            file << bytes
        end

        file.close

        puts "archivo recibido"

        estado_descarga = EstadoDescarga.new estado: 0
        return estado_descarga
    end

    def subir_musica(chunk)
        puts "Recibiendo archivo"
        
        bytesfile = []
        dir = ""

        chunk.each_remote_read do |c|
            
            if c.content.nil? || c.id_escucha.nil? || c.id_artista.nil? || c.id_album.nil? || c.id_cancion.nil?
                raise GRPC::InvalidArgument.new
            end

            dir = "/home/vagrant/bin/res/#{c.id_escucha}/#{c.id_artista}/#{c.id_album}/#{c.id_cancion}"
            FileUtils.mkdir_p(dir) unless Dir.exists?(dir)

            bytesfile << c.content
        end

        file = open(File.join(dir, "music.mp3"), 'wb')

        bytesfile.each do |bytes|
            file << bytes
        end

        file.close
        puts "Archivo recibido"
        estado_descarga = EstadoDescarga.new estado: 0
        return estado_descarga
    end
end

port = '192.168.1.82:50052'
stub = GRPC::RpcServer.new
stub.add_http2_port(port, :this_port_is_insecure)
GRPC.logger.info("... running insecurely in #{port}")
stub.handle(MediaServiceServer.new)
puts 'MediaService runinng'
stub.run_till_terminated_or_interrupted([1, 'int', 'SIGQUIT'])