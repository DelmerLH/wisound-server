DROP DATABASE IF EXISTS wisounddb;

CREATE DATABASE wisounddb;
\c wisounddb

CREATE TABLE escucha (
    id SERIAL,
    correo VARCHAR(60) NOT NULL UNIQUE,
    contrasenia VARCHAR(60) NOT NULL,
    CONSTRAINT PK_escucha PRIMARY KEY (id)
);

CREATE TABLE artista (
    id SERIAL,
    nombre VARCHAR(50) NOT NULL,
    escucha_id INT NOT NULL UNIQUE,
    CONSTRAINT PK_artista PRIMARY KEY (id)
);

CREATE TABLE album (
    id SERIAL,
    nombre VARCHAR(50) NOT NULL,
    anio INT NOT NULL,
    artista_id INT NOT NULL,
    CONSTRAINT PK_album PRIMARY KEY (id)
);

CREATE TABLE cancion (
    id SERIAL,
    nombre VARCHAR(30) NOT NULL,
    genero VARCHAR(30) NOT NULL,
    colaboracion VARCHAR(30),
    album_id INT NOT NULL,
    CONSTRAINT PK_cancion PRIMARY KEY (id)
);

CREATE TABLE playlist (
    id SERIAL,
    nombre VARCHAR(20) NOT NULL,
    privacidad VARCHAR(7),
    escucha_id INT NOT NULL,
    CONSTRAINT PK_playlist PRIMARY KEY (id)
);

CREATE TABLE playlist_cancion (
    playlist_id INT NOT NULL,
    cancion_id INT NOT NULL
);

ALTER TABLE artista
ADD CONSTRAINT FK_artista_escucha FOREIGN KEY (escucha_id)
REFERENCES escucha (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE album
ADD CONSTRAINT FK_album_artista FOREIGN KEY (artista_id)
REFERENCES artista (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE cancion
ADD CONSTRAINT FK_cancion_album FOREIGN KEY (album_id)
REFERENCES album (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE playlist
ADD CONSTRAINT FK_playlist_escucha FOREIGN KEY (escucha_id)
REFERENCES escucha (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE playlist_cancion
ADD CONSTRAINT FK_playlist_cancion_playlist FOREIGN KEY (playlist_id)
REFERENCES playlist (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE playlist_cancion
ADD CONSTRAINT FK_playlist_cancion_cancion FOREIGN KEY (cancion_id)
REFERENCES cancion (id)
ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO escucha (correo, contrasenia) VALUES ('delmrlophz@gmail.com', 'perrito123');
INSERT INTO escucha (correo, contrasenia) VALUES ('delmr_l@hotmail.com', 'perrito123');
INSERT INTO artista (nombre, escucha_id) VALUES ('Los blenders', 1);
INSERT INTO album (nombre, anio, artista_id) VALUES ('Ha sido', 2017, 1);
INSERT INTO cancion (nombre, genero, album_id) VALUES ('Ana Sofi', 'rock', 1);
INSERT INTO cancion (nombre, genero, album_id) VALUES ('Amor prohibido II', 'rock', 1);
INSERT INTO playlist (nombre, privacidad, escucha_id) VALUES ('Las rancheras', 'Abierto', 1);