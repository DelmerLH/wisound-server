require 'pg'
require_relative '../lib/data_service_services_pb'

class AlbumController

    def initialize
        @connection = PG.connect('localhost', 5432, '', '', 'wisounddb', 'postgres', 'perrito123')
    end

    def crear_album(registrar_album_req)
        params = [registrar_album_req.nombre_album, Time.new.year, registrar_album_req.id_artista]
        res =  @connection.exec_params('INSERT into album (nombre, anio, artista_id) VALUES ($1, $2, $3)', params)
        return res.cmd_tuples == 1
    end

    def recuperar_lista_albums(id_artista)
        res =  @connection.exec_params('SELECT id, nombre FROM album WHERE artista_id = $1', [id_artista])

        albums = []
        
        if res.ntuples > 0
            for i in 0..res.ntuples - 1
                album  = Album.new id: res.getvalue(i, 0).to_i, nombre: res.getvalue(i, 1)
                albums << album
            end
        end

        return albums
    end
end