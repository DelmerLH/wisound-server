require 'pg'
require_relative '../lib/data_service_services_pb'

class PlaylistController
    def initialize
        @connection = PG.connect('localhost', 5432, '', '', 'wisounddb', 'postgres', 'perrito123')
    end

    def crear_playlist(crear_playlist_req)
        parameters = [crear_playlist_req.nombre_playlist, crear_playlist_req.privacidad.to_s, crear_playlist_req.escucha_id]
        res = @connection.exec_params('INSERT INTO playlist (nombre, privacidad, escucha_id) VALUES ($1, $2, $3)', parameters)
        return res.cmd_tuples == 1
    end

    def recuperar_playlists(escucha_id)
        res = @connection.exec_params('SELECT id, nombre, privacidad FROM playlist WHERE escucha_id = $1 ORDER BY id', [escucha_id])
        
        playlists = []

        if res.ntuples > 0
            for i in 0..res.ntuples - 1
                playlist = Playlist.new 
                playlist.id = res.getvalue(i, 0).to_i
                playlist.nombre = res.getvalue(i, 1)
                playlist.privacidad = res.getvalue(i, 2)
                playlists << playlist
            end
        end

        return playlists
    end

    def cambiar_privacidad(playlist_id)
        res = @connection.exec_params('SELECT privacidad FROM playlist WHERE id = $1', [playlist_id])
        fue_actualizado = false

        if res.ntuples == 1 
            if res.getvalue(0,0) == "Abierto"
                res = @connection.exec_params('UPDATE playlist SET privacidad = $1 WHERE id = $2 ', ["Cerrado", playlist_id])
                fue_actualizado = true
            else
                res = @connection.exec_params('UPDATE playlist SET privacidad = $1 WHERE id = $2 ', ["Abierto", playlist_id])
                fue_actualizado = true
            end
        end

        return fue_actualizado
    end
end