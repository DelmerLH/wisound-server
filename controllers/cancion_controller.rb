require 'pg'
require_relative '../lib/data_service_services_pb'

class CancionController

    def initialize
        @connection = PG.connect('localhost', 5432, '', '', 'wisounddb', 'postgres', 'perrito123')
    end

    def crear_cancion(registrar_req)
        params = [registrar_req.cancion.nombre, registrar_req.cancion.genero, registrar_req.cancion.colaboracion, registrar_req.id_album]
        res = @connection.exec_params('INSERT INTO cancion (nombre, genero, colaboracion, album_id) VALUES ($1, $2, $3, $4)', params)
        return res.cmd_tuples == 1
    end

    def buscar_cancion(nombre_cancion)
        canciones = []
        artistas = []
        query = 'SELECT cancion.id, cancion.nombre, album.id, album.nombre, artista.id, artista.nombre FROM cancion INNER JOIN album ON cancion.album_id = album.id INNER JOIN artista ON album.artista_id = artista.id WHERE cancion.nombre LIKE $1'
        nombre_cancion = nombre_cancion + "%"
        params = [nombre_cancion]

        res = @connection.exec_params(query, params)

        if res.ntuples > 0
            for i in 0..res.ntuples - 1
                cancion = Cancion.new id: res.getvalue(i, 0).to_i, nombre: res.getvalue(i, 1)
                canciones << cancion
                artista = Artista.new id: res.getvalue(i, 4).to_i, nombre: res.getvalue(i, 5)
                artistas << artista
            end
        end

        return [canciones, artistas]
    end

    def recuperar_cancion(id_cancion)
        res = @connection.exec_params('SELECT * FROM cancion WHERE id = $1', [id_cancion])

        cancion = Cancion.new

        if res.ntuples == 1          
            cancion.id = res.getvalue(0,0)
            cancion.nomre = res.getvalue(0,1)
            cancion.genero = res.getvalue(0,2)
            cancion.colaboracion = res.getvalue(0,3)
        end

        return cancion
    end
end        