require 'pg'
require_relative '../lib/data_service_services_pb'

class ArtistaController

    def initialize
        @connection = PG.connect('localhost', 5432, '', '', 'wisounddb', 'postgres', 'perrito123')
    end

    def recuperar_artista(id_escucha)
        res = @connection.exec_params('SELECT * FROM artista WHERE escucha_id = $1', [id_escucha])
        if res.ntuples == 1
            artista = Artista.new id: res.getvalue(0,0).to_i, nombre: res.getvalue(0,1)
            return artista
        else
            raise ArgumentError.new
        end
    end

    def registrar_artista(registrar_artista_req)
        params = [registrar_artista_req.nombre, registrar_artista_req.id_escucha]
        
        begin
            res = @connection.exec_params('INSERT INTO artista (nombre, escucha_id) VALUES ($1, $2)', params)
            return res.cmd_tuples == 1
        rescue PG::UniqueViolation => e
            raise e
        end
    end

    def verificar_existencia_artista(id_escucha)
        res = @connection.exec_params('SELECT id, escucha_id FROM artista WHERE escucha_id = $1', [id_escucha])
        return res.ntuples == 1
    end
end