require 'pg'
require_relative '../lib/data_service_services_pb'

class EscuchaController

    def initialize
        @connection = PG.connect('localhost', 5432, '', '', 'wisounddb', 'postgres', 'perrito123')
    end

    def registrar_escucha(escucha)
        correo = escucha.correo
        contrasenia = escucha.contrasenia
        begin
            res = @connection.exec_params('INSERT INTO escucha (correo, contrasenia) VALUES ($1, $2)', [correo, contrasenia])
            return res.cmd_tuples == 1
        rescue PG::UniqueViolation => e
            raise e
        end
    end

    def get_escucha(iniciar_sesion_req)
        correo = iniciar_sesion_req.correo
        contrasenia = iniciar_sesion_req.contrasenia
        res = @connection.exec_params('SELECT id, correo FROM escucha WHERE correo = $1 AND contrasenia = $2', [correo, contrasenia])
        if res.ntuples == 1
            escucha = Escucha.new(id: res.getvalue(0,0).to_i, correo: res.getvalue(0,1))
            return escucha
        else
            raise ArgumentError.new "Las credenciales no coinciden"
        end
    end

    def eliminar_escucha(id)
        res = @connection.exec_params('DELETE FROM escucha WHERE id = $1', [id])
        return res.empty?
    end
end