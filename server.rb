this_dir = File.expand_path(File.dirname(__FILE__))
lib_dir = File.join(this_dir, 'lib')
$LOAD_PATH.unshift(lib_dir) unless $LOAD_PATH.include?(lib_dir)

require 'pg'
require 'grpc'
require 'data_service_services_pb'
require_relative 'controllers/escucha_controller'
require_relative 'controllers/playlist_controller'
require_relative 'controllers/cancion_controller'
require_relative 'controllers/artista_controller'
require_relative 'controllers/album_controller'

class DataServiceServer < DataService::Service

    def iniciar_sesion(iniciar_sesion_req, _unused_call)
        
        if iniciar_sesion_req.correo == "" || iniciar_sesion_req.contrasenia == ""
            raise GRPC::InvalidArgument.new
        end

        escucha_controller = EscuchaController.new

        begin
            escucha = escucha_controller.get_escucha(iniciar_sesion_req)
            return escucha  
        rescue ArgumentError => e
            raise GRPC::NotFound.new
        end
    end

    def registrar_escucha(escucha, unused_call)
        escucha_controller = EscuchaController.new
        
        begin
            fue_registrado = escucha_controller.registrar_escucha(escucha)
            return fue_registrado
        rescue PG::UniqueViolation => e
            raise AlreadyExists.new
        end
    end

    def crear_playlist(crear_playlist_req, unused_call)

        if crear_playlist_req.escucha_id.nil? || crear_playlist_req.nombre_playlist == "" || crear_playlist_req.privacidad.nil?
            raise GRPC::InvalidArgument.new 
        end

        playlist_controller = PlaylistController.new
        fue_registrado = false
        fue_registrado = playlist_controller.crear_playlist crear_playlist_req
        registrar_response = RegistrarResponse.new(fue_registrado: fue_registrado)
        return registrar_response    
    end

    def recuperar_playlists(recuperar_playlists_req, unused_call)

        if recuperar_playlists_req.escucha_id.nil?
            raise GRPC::InvalidArgument
        end

        playlist_controller = PlaylistController.new
        playlists = playlist_controller.recuperar_playlists(recuperar_playlists_req.escucha_id)
        recuperar_playlists_res = RecuperarPlaylistsResponse.new playlists: playlists
        return recuperar_playlists_res
    end

    def cambiar_privacidad(playlist, unused_call)
        
        if playlist.id.nil?
            raise GRPC.InvalidArgument.new
        end

        playlist_controller = PlaylistController.new
        fue_actualizado = playlist_controller.cambiar_privacidad(playlist.id)
        actualizar_response = ActualizarResponse.new actualizado: fue_actualizado
        return actualizar_response
    end

    def buscar_canciones(buscar_cancion_req, unused_call)
        if buscar_cancion_req.cancion == ""
            raise GRPC::InvalidArgument.new
        end

        cancion_controller = CancionController.new
        results = cancion_controller.buscar_cancion(buscar_cancion_req.cancion)
        return RecuperarCancionesResponse.new canciones: results[0], artistas: results[1]
    end

    def recuperar_artista(escucha, unused_call)

        if escucha.id.nil?
            raise GRPC::InvalidArgument.new
        end

        artista_controller = ArtistaController.new
        
        begin
            artista = artista_controller.recuperar_artista(escucha.id)
            return artista
        rescue ArgumentError => e
            raise GRPC::NotFound.new
        end
    end

    def registrar_artista(registrar_artista_req, unused_call)
        
        if registrar_artista_req.id_escucha.nil? || registrar_artista_req.nombre == ""
            raise GRPC::InvalidArgument.new
        end

        artista_controller = ArtistaController.new
        begin
            fue_registrado = artista_controller.registrar_artista(registrar_artista_req)
            response = RegistrarResponse.new fue_registrado: fue_registrado
            return response
        rescue PG::UniqueViolation => e
            raise GRPC::AlreadyExists.new
        end
    end

    def verificar_existencia_artista(verificar_existencia_req, unused_call)

        if verificar_existencia_req.id.nil?
            raise GRPC::InvalidArgument.new
        end

        artista_controller = ArtistaController.new
        existe = artista_controller.verificar_existencia_artista(verificar_existencia_req.id)
        response = ActualizarResponse.new actualizado: existe
        return response
    end

    def crear_album(registrar_album_req, unused_call)
        
        if registrar_album_req.id_artista.nil? || registrar_album_req.nombre_album == ""
            raise GRPC::InvalidArgument.new
        end

        album_controller = AlbumController.new
        fue_registrado = album_controller.crear_album(registrar_album_req)
        response = RegistrarResponse.new fue_registrado: fue_registrado
        return response
    end

    def recuperar_albums(artista, unused_call)

        if artista.id.nil?
            raise GRPC::InvalidArgument.new
        end

        album_controller = AlbumController.new
        albums_list = album_controller.recuperar_lista_albums(artista.id)
        recuperar_albums_response = RecuperarAlbumsResponse.new albums: albums_list
        return recuperar_albums_response
    end

    def crear_cancion(registrar_cancion_req, unused_call)
        if registrar_cancion_req.cancion.nombre == "" || registrar_cancion_req.cancion.genero = "" || registrar_cancion_req.id_album.nil?
            raise GRPC::InvalidArgument.new
        end

        cancion_controller = CancionController.new
        fue_registrado = cancion_controller.crear_cancion(registrar_cancion_req)
        registrar_res = RegistrarResponse.new fue_registrado: fue_registrado
        return registrar_res
    end
end

port = '192.168.1.81:50051'
stub = GRPC::RpcServer.new
stub.add_http2_port(port, :this_port_is_insecure)
GRPC.logger.info("... running insucurely in #{port}")
stub.handle(DataServiceServer.new)
puts 'Server running'
stub.run_till_terminated_or_interrupted([1, 'int', 'SIGQUIT'])