require_relative '../controllers/escucha_controller'
require_relative '../lib/data_service_pb'
require 'test/unit'

class EscuchaControllerTest < Test::Unit::TestCase
    
    def test_registrar_escucha
        escucha = Escucha.new(correo: "delmrlophz@gmail.com", contrasenia: "perrito123")
        escucha_controller = EscuchaController.new
        assert_equal true, escucha_controller.registrar_escucha(escucha)
    end

    def test_get_escucha
        iniciar_sesion_req = IniciarSesionRequest.new(correo: "delmrlophz@gmail.com", contrasenia: "perrito123")
        escucha_controller = EscuchaController.new
        escucha = escucha_controller.get_escucha(iniciar_sesion_req)
        assert_equal 1, escucha.id
    end
        
    def test_eliminar_escucha
        iniciar_sesion_req = IniciarSesionRequest.new(correo: "delmrlophz@gmail.com", contrasenia: "perrito123")
        escucha_controller = EscuchaController.new
        escucha = escucha_controller.get_escucha(iniciar_sesion_req)
        filas_afectadas = escucha_controller.eliminar_escucha(escucha.id)
        assert_equal true, filas_afectadas
    end
end